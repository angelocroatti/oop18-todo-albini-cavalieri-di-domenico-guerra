export C_CYAN="\e[1;36m"
export C_RED="\e[1;31m"
export C_GREEN="\e[1;32m"
export C_RESET="\e[0m"

cmd() {
    echo -e "${C_CYAN}>>> running $@${C_RESET}"
    set +euo pipefail
    $@
    ret="$?"
    set -euo pipefail
    if [[ "${ret}" -eq 0 ]]; then
        echo -e "${C_GREEN}>>> command exited with status ${ret}${C_RESET}"
    else
        echo -e "${C_RED}>>> command exited with status ${ret}${C_RESET}"
    fi
    return "${ret}"
}
