#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export PATH="$PATH:/c/Program Files/Java/jdk1.8.0_${ORACLEJDK_VERSION}/bin"
ci/run/common.sh

# Explicitly stop the Gradle daemon otherwise the Travis CI build will hang
# *shrug*
./gradlew --stop
