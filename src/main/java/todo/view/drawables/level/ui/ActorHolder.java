package todo.view.drawables.level.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * This interface represents an object that holds a GDX actor.
 */
public interface ActorHolder {
    /**
     * @return the actor to be added to GDX
     */
    Actor getActor();
}
