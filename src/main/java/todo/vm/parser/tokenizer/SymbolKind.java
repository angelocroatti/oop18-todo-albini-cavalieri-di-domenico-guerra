package todo.vm.parser.tokenizer;

/**
 * This enum represents the accepted kinds of {@link Symbol}.
 */
public enum SymbolKind {
    COLON, NEW_LINE, END_OF_INPUT;
}
